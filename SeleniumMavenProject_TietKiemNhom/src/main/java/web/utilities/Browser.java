package web.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.io.File;

/**
 * Created by TamTran on 12/24/2016.
 */
public class Browser {
    private static String browserType;
    private static String browserPath;
    private static ChromeOptions options;

    public static ThreadLocal<WebDriver> remoteWebDriver = new ThreadLocal<WebDriver>();

    /**
     * Return a browser instance based on parameters above
     */
    public static WebDriver getInstance(String inBrowserType) {

        browserType = inBrowserType;
        browserPath = System.getProperty("user.dir") + File.separator + "resources" + File.separator + "browser-drivers" + File.separator;
        WebDriver driver = buildDriver();
        driver.manage().window().maximize();
        return driver;
    }

    /**
     * Build a "local" browser instance
     */
    private static WebDriver buildDriver() {
        WebDriver driver = null;

        switch (browserType.toLowerCase()) {
            case "firefox":
                System.setProperty("webdriver.gecko.driver", browserPath + "geckodriver.exe");
                driver = new FirefoxDriver();
                break;
            case "safari":
                DesiredCapabilities desiredCapabilities = DesiredCapabilities.safari();
                SafariOptions safariOptions = new SafariOptions();
                safariOptions.setUseCleanSession(true);
                desiredCapabilities.setCapability(SafariOptions.CAPABILITY, safariOptions);
                desiredCapabilities.setCapability(SafariOptions.CAPABILITY, new SafariOptions());
                driver = new SafariDriver(desiredCapabilities);
                break;
            case "chrome":
                System.setProperty("webdriver.chrome.driver", browserPath + "chromedriver.exe");
                System.out.println(browserPath + "chromedriver.exe");
                options = new ChromeOptions();
                options.addArguments("disable-infobars");
                driver = new ChromeDriver(options);
                break;

            case "ie":
                System.setProperty("webdriver.ie.driver", browserPath + "IEDriverServer.exe");
                driver = new InternetExplorerDriver();
                break;
        }

        return driver;
    }

}
